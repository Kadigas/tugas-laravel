<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function home(){
        return view('home');
    }
    function welcome(Request $request){
        //dd($request->all());
        $fname = $request["fname"];
        $lname = $request["lname"];
        return view('welcome', compact('fname', 'lname'));
    }
}
