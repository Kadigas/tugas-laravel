<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gndr">Male<br>
        <input type="radio" name="gndr">Female<br>
        <input type="radio" name="gndr">Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nation">
            <option value="1">Indonesian</option>
            <option value="2">American</option>
            <option value="3">Australian</option>
            <option value="4">British</option>
            <option value="5">Malaysian</option>
            <option value="6">Other</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>