@extends('layouts.master')

@section('title')
    List Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-success btn-sm">+New</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">No. </th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $value)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>
                    <a href="/cast/{{$value->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                    <form action="/cast/{{$value->id}}" method="POST" style="display: inline">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection