@extends('layouts.master')

@section('title')
    Detail Cast
@endsection

@section('content')
<h4>{{$cast->nama}}</h4>
<h5>Age: {{$cast->umur}}</h5>
<h5>Bio:</h5>
<p>{{$cast->bio}}</p>
@endsection