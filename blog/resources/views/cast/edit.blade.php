@extends('layouts.master')

@section('title')
    Edit Cast: {{$cast->nama}}
@endsection

@section('content')
<div>
   <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Name</label>
            <input type="text" value="{{$cast->nama}}"" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">            
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Age</label>
            <input type="text" value="{{$cast->umur}}" class="form-control" name="umur" id="body" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Bio</label>
            <textarea class="form-control" name="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
</div>
@endsection